
"*****************************************************************************
"" Vim-PLug core
"*****************************************************************************
if has('vim_starting')
  set nocompatible               " Be iMproved
endif

let vimplug_exists=expand('~/.vim/autoload/plug.vim')

let g:vim_bootstrap_langs = "python"
let g:vim_bootstrap_editor = "vim"				" nvim or vim

if !filereadable(vimplug_exists)
  echo "Installing Vim-Plug..."
  echo ""
  silent !\curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  let g:not_finish_vimplug = "yes"

  " Run shell script if exist on custom select language

  autocmd VimEnter * PlugInstall
endif


let $VIMHOME = expand('<sfile>:p:h')
let s:featuresVimPath = $VIMHOME.'/features.vim'
if filereadable(s:featuresVimPath)
  execute 'source '.fnameescape(s:featuresVimPath)
endif


" Required:
call plug#begin(expand('~/.vim/plugged'))

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'felixhummel/setcolors.vim'
Plug 'BeeWarloc/jellybeans.vim'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ervandew/supertab'
Plug 'tpope/vim-dispatch'
Plug 'scrooloose/syntastic'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-surround'

let g:easytags_suppress_ctags_warning = 1
Plug 'xolox/vim-easytags'
Plug 'xolox/vim-misc'

" Changing cursor
Plug 'jszakmeister/vim-togglecursor'

" Temporary placement
Plug 'BeeWarloc/vim-fuse'

" Whiteline stuff
Plug 'editorconfig/editorconfig-vim'
Plug 'tpope/vim-sleuth'

" Easily search for, substitute and abbreviate multiple variants of word
" Case-preserving replace: 
"    :%S/badjob/goodjob/g
Plug 'tpope/vim-abolish'

" Dotnet support
if get(g:, 'feature_omnisharp', 0)
  Plug 'OmniSharp/omnisharp-vim'
endif

" Rust support
if get(g:, 'feature_rust', 0)
  Plug 'rust-lang/rust.vim'
  Plug 'racer-rust/vim-racer'
endif

Plug 'kien/rainbow_parentheses.vim'
Plug 'nathanaelkane/vim-indent-guides'

call plug#end()


"*****************************************************************************
"" Basic Setup
"*****************************************************************************"
"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

"" Fix backspace indent
set backspace=indent,eol,start

set fillchars+=vert:│

"" Tabs. May be overriten by autocmd rules
set tabstop=4
set softtabstop=0
set shiftwidth=4
" set expandtab

"" Map leader to ,
let mapleader=','

"" Enable hidden buffers
set hidden

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

"" Encoding
" set bomb
set ttyfast

"" Left or right cursor at line boundaries wrap to next/prev line
set whichwrap+=<,>,h,l,[,]

"" Directories for swp files
set nobackup
set noswapfile

set fileformats=unix,dos,mac
set showcmd
set shell=/bin/sh

" session management
let g:session_directory = "~/.vim/session"
let g:session_autoload = "no"
let g:session_autosave = "no"
let g:session_command_aliases = 1

" KARSTENS SETTINGS


let g:airline_theme='badwolf'

" Rainbow parentheses, improved colors
let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['63',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['115',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['63',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces


au VimEnter * IndentGuidesEnable

" Fuse
au BufNewFile,BufRead *.unoproj set filetype=json

" NERDTree
"  Show current file in tree: ctrl-l
noremap <C-l> :NERDTreeFind<CR>
"  Quit on open
let NERDTreeQuitOnOpen=1


" Only list files not ignored by git
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

" Automatically displays all buffers when there's only one tab open.
" This is disabled by default; add the following to your vimrc to enable the extension:
let g:airline#extensions#tabline#enabled = 1


set t_Co=256
colorscheme jellybeans


set listchars=tab:>-,trail:~,extends:>,precedes:<
set list

" mouse
set mouse=a
if &term =~ '^screen'
    " tmux knows the extended mouse mode
    set ttymouse=xterm2
endif

" Highlight cursorline
set cursorline

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set tabstop=4
set smartindent
set shiftwidth=4

" syntastic
let g:syntastic_javascript_checkers = ['jshint']
let g:syntastic_ux_checkers = ['xmllint']

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


" open current file in windows explorer
noremap <C-e> :silent !~/.vim/tools/open-in-explorer.exe '%'<CR>:redraw!<CR>


" Include omnisharp.vim if feature turned on
if get(g:, 'feature_omnisharp', 0)
  source $VIMHOME/omnisharp.vim
endif

" Include rust.vim if feature turned on
if get(g:, 'feature_rust', 0)
  source $VIMHOME/rust.vim
endif

