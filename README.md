# Karstens .vim dir

## Random cheat sheet

### VIM

* Fix tabs: gg=G (gg goes to the top of the file, = is a command to fix the indentation and G tells it to perform the operation to the end of the file)
* Delete upto character x: dtx
* Delete upto including character x: dfx
* Change upto character x: ctx
* Back: `<C-o>`
* Forward: `<C-i>`
* Alternative ESC: <C-c>
* XML, change element name: `cst<newname` (then enter without closing `>`)
* XML, select tag block: `vit`
* XML, select tag block with tag: `vat`
* Surround selection with (VISUAL LINE): S<tag>
* CasingAware abolish.vim substition `%S/foo/bar/g`. Will transform `foo Foo FOO` to `bar Bar BAR`.

#### Ctrl-P

* Filename search: `<C-d>`
* Regex search: `<C-r>`

### TMUX

* zoom toggle : `<prefix> z`
* scroll      : `<prefix> [`
* multiple monitor session: tmux new-session -t [session-id]
* C-a C-o        rotate window ‘up’ (i.e. move all panes)

## Setup

### Syntastic JS checking
```
npm install -g jshint
```

### TMUX

`ln -s ~/.vim/.tmux.conf ~/.tmux.conf`

### MSYS2 setup

Change to the following settings at top of `msys2_shell.cmd`:

```
rem To activate windows native symlinks uncomment next line
set MSYS=winsymlinks:nativestrict

rem Set debugging program for errors
rem set MSYS=error_start:%WD%../../mingw64/bin/qtcreator.exe^|-debug^|^<process-id^>

rem To export full current PATH from environment into MSYS2 use '-use-full-path' parameter
rem or uncomment next line
set MSYS2_PATH_TYPE=inherit
```

### Mac iTerm2 setup

To get Alt- modifier key working correctly set "Profile -> Keys -> Left option key" to "Esc+"

