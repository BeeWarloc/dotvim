#!/bin/bash

if [ -n "$TMUX_VERSION_MAJOR" ] && [ -S "$HOME/.ssh/ssh_auth_sock.$(hostname)" ]; then
    SSH_AUTH_SOCK="$HOME/.ssh/ssh_auth_sock.$(hostname)"
    export SSH_AUTH_SOCK
fi

export PATH="$HOME/.vim/scripts:$PATH"

ping_until_up()
{
    echo -n "Waiting for $*.."
    while true; do
        ping -W1 -c1 "$@" < /dev/null >> /dev/null 2> /dev/null && break
        echo -n .
        sleep 1
    done
    echo ".done"
    sleep 3
}

cdws() {
    if [ -z "$VSCODE_WS" ]; then
        echo "No VSCODE_WS env var set, not running in vscode terminal?" 2>&1
        return 1
    fi
    cd "$VSCODE_WS"
}

cdtop() { local start="${1:-$PWD}"; while [ ! -d "${start}/.git" ]; do start="$(dirname "$start")"; done; cd "$start"; }

path_use() {
    if [ $# -lt 1 ] || [ $# -gt 2 ]; then
        echo "Usage: path_use <dir> [prompt prefix]" 1>&2
        return 1
    fi
    local dir="$1"
    local ps_prefix="$2"

    if [ -d "$dir" ]; then
        export PATH="$dir:$PATH"
    else
        echo "Directory $2 does not exist" 1>&2
        return 1
    fi

    if [ -z "$ps_prefix" ]; then
        ps_prefix="$(basename "$dir")"
        if [ "$ps_prefix" = "bin" ]; then
            ps_prefix="$(basename "$(dirname "$dir")")"
        fi
        if [ -z "$ps_prefix" ]; then
            ps_prefix="altered PATH"
        fi
    fi

    export PS1="($ps_prefix) $PS1"
}

# git aliases

# use complete-alias for completion https://github.com/cykerway/complete-alias
. "$HOME/.vim/complete-alias/complete_alias"
alias gd="git diff"
complete -F _complete_alias gd
alias gg="git grep"
complete -F _complete_alias gg
alias g=git
complete -F _complete_alias g
alias gco="git checkout"
complete -F _complete_alias gco
alias gst="git status"
complete -F _complete_alias gst
